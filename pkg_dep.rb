#!/usr/bin/ruby

require 'optparse'
require 'set'

def read_file(file_name)
    ret_val = Hash.new()

    File.open(file_name).each_line do |line|
        entry = line.chomp.split(":")
        ret_val[entry[0]] = entry[1].split(",")
    end

    return ret_val
end

def get_deps(package_name, dep_hash, checked_packages = nil)
    dependencies = Set.new()

    if checked_packages.nil?
        checked_packages = Set.new()
    end

    dep_hash[package_name].each do |dependency|
        if checked_packages.include?(dependency)
            checked_packages.add(package_name)
            puts("Detected Circular Dependencies!\n\t" + checked_packages.to_a.join(","))
            exit
        end

        checked_packages.add(package_name)
        dependencies.add(dependency)
        if dep_hash.has_key?(dependency)
            dependencies.merge(get_deps(dependency, dep_hash, checked_packages))
        end
    end

    return dependencies.to_a
end

Options = Struct.new(:dep_file, :package_name)

class Parser
    def self.parse(options)
        args  = Options.new("dep_solver")

        opt_parser = OptionParser.new do |opts|
            opts.banner = "Usage: #{File.basename($0)} [options]"

            opts.on("-h", "--help", "Displays Help") do
                puts opts
                exit
            end
            opts.on("-dDEP_FILE", "--dep-file=DEP_FILE", "File listing dependencies") do |d|
                args.dep_file = File.expand_path(d)
            end
            opts.on("-pPACKAGE", "--package=PACKAGE", "Solve dependencies for this package") do |p|
                args.package_name = p
            end
        end

        opt_parser.parse!(options)

        return args
    end
end

if ARGV.empty?
    Parser.parse %w[-h]
end
options = Parser.parse(ARGV)

dependency_hash = read_file(options.dep_file)
dependencies = get_deps(options.package_name, dependency_hash)

puts("Package #{options.package_name} depends on:\n\t" + dependencies.join(","))
